const mongoose = require("mongoose");

const connectDb = async () => {
  console.log(process.env.MONGO_URL, "uri");
  try {
    await mongoose.connect(process.env.MONGO_URL);
    console.log("DB is connected!");
  } catch (error) {
    console.log("MONGO ERROR: ", error.message);
  }
};

connectDb();
