require("dotenv").config();

const express = require("express");

// Middlewares
const cors = require("cors");
const { notFound, errorHandler } = require("./middlewares/error.middlewares");

// DB
require("./config/db");

// Routes
const chatRoutes = require("./routes/chat.route");
const userRoutes = require("./routes/user.route");
const MessageRoutes = require("./routes/message.route");

const app = express();

// MIDDLEWARES
app.use(cors());
app.use(express.json());

// ROUTES
app.get("/", (req, res) => {
  res.send("API IS WORKING!");
});

app.use("/api/chat", chatRoutes);
app.use("/api/user", userRoutes);
app.use("/api/messages", MessageRoutes);

app.use(notFound);
app.use(errorHandler);

// INITIALIZATION
const PORT = process.env.PORT || 3000;

const server = app.listen(PORT, () => console.log(`Server on port ${PORT}!`));

const io = require("socket.io")(server, {
  pingTimeout: 60000,
  cors: {
    origin: "http://localhost:5173",
  },
});

io.on("connection", (socket) => {
  console.log("Connected to socket.io");

  socket.on("setup", (user) => {
    socket.join(user._id);
    socket.emit("connected");
  });

  socket.on("join chat", (room) => {
    socket.join(room);
  });

  socket.on("leave chat", (room) => {
    if (socket.rooms.size > 2) {
      socket.leave(Array.from(socket.rooms.values())[socket.rooms.size - 1]);
    }
  });

  socket.on("typing", (room) => socket.in(room).emit("typing", room));

  socket.on("stop typing", (room) => socket.in(room).emit("stop typing"));

  socket.on("new message", (newMessage) => {
    let chat = newMessage.chat;

    if (!chat.users) console.log("chat.users not defined");

    chat.users.forEach((user) => {
      if (user._id === newMessage.sender._id) return;

      socket.in(user._id).emit("message recieved", newMessage);
    });
  });

  socket.off("setup", () => {
    console.log("USER DISCONNECTED");
    socket.leave(userData._id);
  });
});
