module.exports = success = (res, status, body, error) => {
  return res.status(status).json({
    body,
    error,
  });
};
