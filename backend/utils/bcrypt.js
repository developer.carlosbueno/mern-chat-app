const bcrypt = require("bcryptjs");

const encrypt = (text) => {
  return bcrypt.hashSync(text, 10);
};

const compare = (text, hash) => {
  return bcrypt.compareSync(text, hash);
};

module.exports = {
  encrypt,
  compare,
};
