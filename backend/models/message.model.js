const { Schema, model } = require('mongoose')

const messageSchema = new Schema(
  {
    sender: {
      type: Schema.Types.ObjectId,
      ref: 'users',
    },
    chat: {
      type: Schema.Types.ObjectId,
      ref: 'chats',
    },
    content: { type: String, trim: true },
  },
  {
    timestamps: true,
  }
)

module.exports = model('messages', messageSchema)
