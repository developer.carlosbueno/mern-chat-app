const { Schema, model } = require("mongoose");

const chatSchema = new Schema(
  {
    chatName: { type: String, trim: true },
    isGroupChat: { type: Boolean, default: false },
    users: [
      {
        type: Schema.Types.ObjectId,
        ref: "users",
      },
    ],
    latestMessage: {
      type: Schema.Types.ObjectId,
      ref: "messages",
    },
    groupAdmin: {
      type: Schema.Types.ObjectId,
      ref: "users",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("chats", chatSchema);
