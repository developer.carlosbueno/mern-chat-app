const jwt = require('jsonwebtoken')
const User = require('../models/users.model')
const response = require('../utils/response')

const protect = async (req, res, next) => {
  let authorization = req.headers.authorization

  if (authorization && authorization.startsWith('Bearer')) {
    let token = authorization.split(' ')[1]

    let decoded = jwt.verify(token, process.env.JWT_SECRET)

    try {
      req.user = await User.findById(decoded.id).select('-password')
      next()
    } catch (error) {
      response(res, 401, 'Token not working, no authorize', true)
    }
  } else {
    response(res, 401, 'No token no authorized', true)
  }
}

module.exports = protect
