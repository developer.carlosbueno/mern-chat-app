// Models
const User = require('../models/users.model')

// Utils
const response = require('../utils/response')
const generateToken = require('../utils/jwt')
const { encrypt, compare } = require('../utils/bcrypt')

const registerUser = async (req, res) => {
  const { name, email, password, pic } = req.body

  if (!name || !email || !password) {
    return response(res, 400, 'Please enter all the fields', true)
  }

  try {
    const userExist = await User.findOne({ email })

    if (userExist) {
      return response(res, 400, 'User already exists', true)
    }

    const user = await User.create({
      name,
      email,
      password: encrypt(password),
      pic,
    })

    response(res, 201, { user, token: generateToken(user._id) }, false)
  } catch (error) {
    console.log('Creating user error: ', error.message)
    response(res, 500, `"Creating user error: ", ${error.message}`)
  }
}

const authUser = async (req, res) => {
  const { email, password } = req.body

  try {
    const user = await User.findOne({ email })
    if (user && (await compare(password, user.password))) {
      response(res, 200, { user, token: generateToken(user._id) })
    } else {
      throw new Error('Wrong email or password.')
    }
  } catch (error) {
    console.log('Login user error: ', error)
    response(res, 500, error.message, true)
  }
}

const getAllUsers = async (req, res) => {
  const keyword = req.query.search
    ? {
        $or: [
          { name: { $regex: req.query.search, $options: 'i' } },
          { email: { $regex: req.query.search, $options: 'i' } },
        ],
      }
    : {}

  try {
    const users = await User.find(keyword).find({ _id: { $ne: req.user._id } })
    response(res, 200, users, false)
  } catch (error) {
    console.log('GET ALL USERS ERROR: ', error.message)
    response(res, 500, error.message, true)
  }
}

module.exports = {
  registerUser,
  authUser,
  getAllUsers,
}
