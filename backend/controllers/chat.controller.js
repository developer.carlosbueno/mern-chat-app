// Utils
const response = require('../utils/response')

// MODELS
const Chat = require('../models/chat.model')
const User = require('../models/users.model')

const accessChat = async (req, res) => {
  const { userId } = req.body

  try {
    if (!userId) {
      throw new Error('No user chat selected')
    }

    let isChat = await Chat.find({
      isGroupChat: false,
      $and: [
        { users: { $elemMatch: { $eq: req.user._id } } },
        { users: { $elemMatch: { $eq: userId } } },
      ],
    })
      .populate('users', '-password')
      .populate('latestMessage')

    isChat = await User.populate(isChat, {
      path: 'lastestMessage.sender',
      select: 'name pic email',
    })

    if (isChat.length > 0) {
      response(res, 200, isChat[0], false)
    } else {
      const newChat = await Chat.create({
        chatName: 'sender',
        isGroupChat: false,
        users: [req.user._id, userId],
      })

      const fullchat = await Chat.findById(newChat._id).populate(
        'users',
        '-password'
      )

      response(res, 201, fullchat, false)
    }
  } catch (error) {
    response(res, 400, error.message, true)
  }
}

const getAllChats = async (req, res) => {
  try {
    const Chats = await Chat.find({
      users: { $elemMatch: { $eq: req.user._id } },
    })
      .populate('users', '-password')
      .populate('groupAdmin', '-password')
      .populate('latestMessage')
      .sort({ updatedAt: -1 })

    let chats = await User.populate(Chats, {
      path: 'latestMessage.sender',
      select: 'name pic email',
    })

    response(res, 200, chats, false)
  } catch (error) {
    console.log(error)
    response(res, 200, error.message, false)
  }
}

const createGroupChat = async (req, res) => {
  let { users, name } = req.body

  console.log(users, 1)

  if (!users || !name) {
    response(res, 400, 'Please fill all the fields', true)
  }

  users.push(req.user._id)
  console.log(users, 2)

  if (users.length <= 2) {
    response(
      res,
      400,
      'More than 2 users are required to form a group chat!',
      true
    )
  }

  try {
    const createdGroup = await Chat.create({
      chatName: name,
      users,
      isGroupChat: true,
      groupAdmin: req.user._id,
    })

    let fullGroupChat = await Chat.findById(createdGroup._id)
      .populate('users', '-password')
      .populate('groupAdmin', '-password')

    response(res, 201, fullGroupChat, false)
  } catch (error) {
    response(res, 500, error.message, true)
  }
}

const renameGroupChat = async (req, res) => {
  const { chatId, name } = req.body

  if (!name) {
    response(res, 400, 'You must fill the name field', true)
  }
  try {
    const updatedGroup = await Chat.findByIdAndUpdate(
      chatId,
      {
        chatName: name,
      },
      {
        new: true,
      }
    )
      .populate('users', '-password')
      .populate('groupAdmin', '-password')

    if (!updatedGroup) {
      throw new Error('Chat group dosent exist')
    }

    response(res, 200, updatedGroup, false)
  } catch (error) {
    response(res, 500, error.message, true)
  }
}

const removeToGroup = async (req, res) => {
  const { chatId, userId } = req.body

  try {
    const removed = await Chat.findByIdAndUpdate(
      chatId,
      {
        $pull: { users: userId },
      },
      {
        new: true,
      }
    )
      .populate('users', '-password')
      .populate('groupAdmin', '-password')

    if (!removed) {
      throw new Error('Chat not found!')
    }

    response(res, 200, removed, true)
  } catch (error) {
    response(res, 200, error.message, true)
  }
}
const addToGroup = async (req, res) => {
  const { chatId, userId } = req.body

  try {
    const added = await Chat.findByIdAndUpdate(
      chatId,
      {
        $push: { users: userId },
      },
      {
        new: true,
      }
    )
      .populate('users', '-password')
      .populate('groupAdmin', '-password')

    if (!added) {
      throw new Error('Chat not found!')
    }

    response(res, 200, added, true)
  } catch (error) {
    response(res, 200, error.message, true)
  }
}

module.exports = {
  accessChat,
  getAllChats,
  createGroupChat,
  renameGroupChat,
  addToGroup,
  removeToGroup,
}
