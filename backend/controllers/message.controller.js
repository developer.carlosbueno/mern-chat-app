const response = require('../utils/response')

// Models
const Message = require('../models/message.model')
const User = require('../models/users.model')
const Chat = require('../models/chat.model')

const createMessage = async (req, res) => {
  try {
    const newMessage = {
      sender: req.user._id,
      content: req.body.content,
      chat: req.body.chat,
    }

    let message = await Message.create(newMessage)

    message = await message.populate('sender', 'name pic')
    message = await message.populate('chat')
    message = await User.populate(message, {
      path: 'chat.users',
      select: 'name pic',
    })

    await Chat.findByIdAndUpdate(req.body.chat, {
      latestMessage: message,
    })

    response(res, 201, message, false)
  } catch (error) {
    response(res, 500, error.message, true)
  }
}

const getMessages = async (req, res) => {
  try {
    const messages = await Message.find({ chat: req.params.id }).populate(
      'sender',
      'name pic'
    )

    response(res, 200, messages, false)
  } catch (error) {
    console.log('GET MESSAGES ERROR: ', error.message)
    response(res, 500, error.message, true)
  }
}

module.exports = {
  createMessage,
  getMessages,
}
