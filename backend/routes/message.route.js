const router = require('express').Router()

// Controller
const {
  createMessage,
  getMessages,
} = require('../controllers/message.controller')
const protect = require('../middlewares/auth.middlewares')

router.post('/', protect, createMessage)
router.get('/:id', protect, getMessages)

module.exports = router
