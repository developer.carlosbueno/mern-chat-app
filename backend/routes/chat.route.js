const router = require('express').Router()

const protect = require('../middlewares/auth.middlewares')

// CONTROLLERS
const {
  getAllChats,
  accessChat,
  createGroupChat,
  renameGroupChat,
  removeToGroup,
  addToGroup,
} = require('./../controllers/chat.controller')

router.use(protect)
router.get('/', protect, getAllChats)

router.post('/', accessChat)
router.post('/group', createGroupChat)

router.patch('/rename', renameGroupChat)
router.patch('/group-remove', removeToGroup)
router.patch('/group-add', addToGroup)

module.exports = router
