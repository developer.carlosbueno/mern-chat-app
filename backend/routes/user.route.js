const router = require('express').Router()

// Controller
const {
  registerUser,
  authUser,
  getAllUsers,
} = require('../controllers/user.controller')
const protect = require('../middlewares/auth.middlewares')

router.post('/', registerUser)
router.post('/login', authUser)
router.get('/', protect, getAllUsers)

module.exports = router
