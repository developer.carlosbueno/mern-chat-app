import { Routes, Route } from 'react-router-dom'
import ChatProvider from './context/ChatProvider'

// Pages
import { Chat } from '@/pages/Chat'
import { Login } from '@/pages/Login'

function App() {
  return (
    <ChatProvider>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/chats' element={<Chat />} />
      </Routes>
    </ChatProvider>
  )
}

export default App
