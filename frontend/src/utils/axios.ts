import axiosModule from "axios";

const axios = axiosModule.create({
  baseURL: "http://localhost:3000",
});

export default axios;
