import { UseToastOptions, AlertStatus } from "@chakra-ui/react";

export const toastConfig = (
  title: string,
  status: AlertStatus
): UseToastOptions => {
  return {
    title,
    status,
    duration: 4000,
    isClosable: true,
    position: "bottom",
  };
};
