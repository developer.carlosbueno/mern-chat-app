import {
  FC,
  ReactNode,
  createContext,
  useState,
  useContext,
  useEffect,
} from "react";

import { User } from "@/models/User";
import { useNavigate } from "react-router-dom";
import { Chat } from "@/models/Chat";

interface InitialProps {
  user: User | null;
  chats: any[];
  setChats: any;
  selectedChat: Chat | null;
  setSelectedChat: any;
  notifications: any[];
  setNotifications: any;
}

let initialState: InitialProps = {
  user: null,
  chats: [],
  setChats: "",
  selectedChat: null,
  setSelectedChat: "",
  notifications: [],
  setNotifications: "",
};

const ChatContext = createContext<InitialProps>(initialState);

interface ChatProviderProps {
  children: ReactNode;
}

export const ChatProvider: FC<ChatProviderProps> = ({ children }) => {
  const navigate = useNavigate();

  const [user, setUser] = useState<User | null>(initialState.user);
  const [chats, setChats] = useState<Chat[]>([]);
  const [selectedChat, setSelectedChat] = useState<Chat | null>(null);
  const [notifications, setNotifications] = useState<any[]>([]);

  useEffect(() => {
    if (localStorage.getItem("user")) {
      let user: User = JSON.parse(localStorage.getItem("user") || "");
      console.log(user, "users");

      setUser(user);
    } else {
      navigate("/");
    }
  }, [localStorage.getItem("user")]);

  return (
    <ChatContext.Provider
      value={{
        user,
        chats,
        setChats,
        selectedChat,
        setSelectedChat,
        notifications,
        setNotifications,
      }}
    >
      {children}
    </ChatContext.Provider>
  );
};

export const chatState = () => {
  return useContext(ChatContext);
};

export default ChatProvider;
