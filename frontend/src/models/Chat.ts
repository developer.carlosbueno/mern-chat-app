import { User } from "./User"

export interface Chat {
  _id: string
  chatName: string,
  isGroupChat: string
  users: User[]
  groupAdmin: User
}