export interface Response<T> {
  error: boolean;
  body: T;
}
