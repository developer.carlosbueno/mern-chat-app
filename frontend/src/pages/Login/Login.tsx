import React, { useEffect } from 'react'

// Components
import {
  Box,
  Container,
  Tabs,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Text,
} from '@chakra-ui/react'

// Styles
import styles from './styles/Login.module.css'
import LoginForm from './components/LoginForm'
import SignupForm from './components/SignupForm'
import { useNavigate } from 'react-router-dom'

export interface LoginProps {}

const Login: React.FC<LoginProps> = () => {
  const navigate = useNavigate()

  useEffect(() => {
    if (localStorage.getItem('user')) {
      navigate('chats')
    }
  }, [])

  return (
    <div className={styles.login}>
      <Container maxW='xl' centerContent>
        <Box
          display='flex'
          justifyContent='center'
          p='3'
          bg='white'
          w={'100%'}
          borderRadius='lg'
          m='40px 0 15px 0'
          borderWidth={'1px'}
        >
          <Text fontSize={'4xl'} fontFamily='Work sans' color={'black'}>
            Talk-A-Tive
          </Text>
        </Box>
        <Box p='4' bg='white' w={'100%'} borderRadius='lg' borderWidth={'1px'}>
          <Tabs variant='soft-rounded'>
            {/* Tabs items */}
            <TabList mb='1rem'>
              <Tab w='50%'>Login</Tab>
              <Tab w='50%'>Sign up</Tab>
            </TabList>

            {/* Tabs content */}
            <TabPanels>
              <TabPanel>
                <LoginForm />
              </TabPanel>
              <TabPanel>
                <SignupForm />
              </TabPanel>
            </TabPanels>
          </Tabs>
        </Box>
      </Container>
    </div>
  )
}

export default Login
