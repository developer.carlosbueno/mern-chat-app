import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useToast } from "@chakra-ui/react";

// SERVICES
import { loginUser } from "../services";

// Components
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  VStack,
} from "@chakra-ui/react";

//UTILS
import { toastConfig } from "@/utils/toastConfig";

export interface LoginFormProps {}

const LoginForm: React.FC<LoginFormProps> = () => {
  const navigate = useNavigate();
  const toast = useToast();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClick = () => setShow(!show);

  const submitHandler = async () => {
    setLoading(true);
    if (!email || !password) {
      toast(toastConfig("Please Fill all the Feilds", "warning"));
      setLoading(false);
      return;
    }

    try {
      await loginUser({ email, password });
      setLoading(false);

      toast(toastConfig("Has iniciado seccion", "success"));
      navigate("chats");
      // navigate("chats");
    } catch (error: any) {
      toast(toastConfig(error.message, "error"));
      setLoading(false);
    }
  };

  return (
    <VStack spacing="0.8rem">
      <FormControl id="email" isRequired>
        <FormLabel>Email</FormLabel>
        <Input
          placeholder="Enter your email"
          onChange={(e) => setEmail(e.target.value)}
          value={email}
        />
      </FormControl>

      <FormControl id="password" isRequired>
        <FormLabel>Password</FormLabel>
        <InputGroup>
          <Input
            type={show ? "text" : "password"}
            placeholder="Enter your password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
          <InputRightElement width="4.5rem">
            <Button h="1.75rem" size="sm" onClick={handleClick}>
              {show ? "Hide" : "Show"}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <Button
        colorScheme="blue"
        w="100%"
        onClick={submitHandler}
        style={{ marginTop: 20 }}
        isLoading={loading}
      >
        Login
      </Button>
      <Button
        colorScheme="red"
        w="100%"
        onClick={() => {
          setEmail("test@example.com");
          setPassword("123456");
        }}
      >
        Get Guest User Credentials
      </Button>
    </VStack>
  );
};

export default LoginForm;
