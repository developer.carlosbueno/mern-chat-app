import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useToast } from "@chakra-ui/react";

// Services
import { registerUser } from "../services";

// Components
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  VStack,
} from "@chakra-ui/react";
import { toastConfig } from "@/utils/toastConfig";

export interface SignupFormProps {}

const SignupForm: React.FC<SignupFormProps> = () => {
  const navigate = useNavigate();
  const toast = useToast();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [confirmpassword, setConfirmpassword] = useState("");
  const [password, setPassword] = useState("");
  const [pic, setPic] = useState("");
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);

  const handleClick = () => setShow(!show);

  const postDetails = (pics: any) => {};

  const submitHandler = async () => {
    setLoading(true);
    if (!name || !email || !password || !confirmpassword) {
      toast(toastConfig("Please Fill all the Feilds", "warning"));
      setLoading(false);
      return;
    }

    if (password !== confirmpassword) {
      toast(toastConfig("Passwords do not match!", "warning"));
      setLoading(false);
      return;
    }

    try {
      await registerUser({ email, name, password });
      toast(toastConfig("Usuario creado correctamente!", "success"));
      setLoading(false);
      navigate("chats");
    } catch (error: any) {
      toast(toastConfig(error.message, "error"));
      setLoading(false);
    }
  };

  return (
    <VStack spacing="0.8rem">
      <FormControl id="name" isRequired>
        <FormLabel>Name</FormLabel>
        <Input
          placeholder="Enter your name"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
      </FormControl>

      <FormControl id="email" isRequired>
        <FormLabel>Email</FormLabel>
        <Input
          placeholder="Enter your email"
          onChange={(e) => setEmail(e.target.value)}
          value={email}
        />
      </FormControl>

      <FormControl id="password" isRequired>
        <FormLabel>Password</FormLabel>
        <InputGroup>
          <Input
            type={show ? "text" : "password"}
            placeholder="Enter your password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
          <InputRightElement width="4.5rem">
            <Button h="1.75rem" size="sm" onClick={handleClick}>
              {show ? "Hide" : "Show"}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <FormControl id="confirmPassworde" isRequired>
        <FormLabel>Confirm Password</FormLabel>
        <InputGroup>
          <Input
            type={show ? "text" : "password"}
            placeholder="Confirm your password"
            onChange={(e) => setConfirmpassword(e.target.value)}
            value={confirmpassword}
          />
          <InputRightElement width="4.5rem">
            <Button h="1.75rem" size="sm" onClick={handleClick}>
              {show ? "Hide" : "Show"}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <FormControl id="picture" isRequired>
        <FormLabel>Upload your pic</FormLabel>
        <Input
          type="file"
          placeholder="Enter your name"
          onChange={(e) => postDetails(e.target.files![0])}
          value={pic}
        />
      </FormControl>

      <Button
        colorScheme="blue"
        w="100%"
        onClick={submitHandler}
        style={{ marginTop: 20 }}
        isLoading={loading}
      >
        Sign Up
      </Button>
    </VStack>
  );
};

export default SignupForm;
