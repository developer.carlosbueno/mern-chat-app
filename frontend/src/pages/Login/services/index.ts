// Models
import { Response } from "@/models/Response";
import { User } from "@/models/User";

// Utils
import axios from "@/utils/axios";

const saveUserToLocalstorage = (data: any) => {
  localStorage.setItem("user", JSON.stringify({ ...data.body.user, token: data.body.token }));
}

export const registerUser = async (body: any) => {
  try {
    const { data } = await axios.post("/api/user", body);

    if (data.error) {
      throw new Error("Ha ocurrido un error");
    }

    saveUserToLocalstorage(data)
    return data;
  } catch (error: any) {
    console.log(error.message);
    return error.message;
  }
};

export const loginUser = async (body: any) => {
  try {
    const { data } = await axios.post<Response<User>>("/api/user/login", body);

    saveUserToLocalstorage(data)
    return data;
  } catch (error: any) {
    console.log(error, "errrr");
    throw new Error(error.response.data.body || error.message);
  }
};
