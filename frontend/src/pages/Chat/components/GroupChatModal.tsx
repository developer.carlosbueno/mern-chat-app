import React, { useState } from 'react'

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  useDisclosure,
  FormControl,
  Input,
  useToast,
  Box,
} from '@chakra-ui/react'
import UserListItem from './UserListItem'
import UserBadgeItem from './UserBadgeItem'
import { User } from '@/models/User'
import ChatLoading from './ChatLoading'
import { createChatGroup, getUsers } from '../services'
import { chatState } from '@/context/ChatProvider'
import { toastConfig } from '@/utils/toastConfig'

export interface GroupChatModalProps {
  children: React.ReactNode
  fetchAgain: boolean
  setFetchAgain: any
}

const GroupChatModal: React.FC<GroupChatModalProps> = ({
  children,
  fetchAgain,
  setFetchAgain,
}) => {
  const { user, setSelectedChat } = chatState()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const toast = useToast()

  const [groupChatName, setGroupChatName] = useState('')
  const [selectedUsers, setSelectedUsers] = useState<User[]>([])
  const [search, setSearch] = useState('')
  const [searchResult, setSearchResult] = useState<User[]>([])
  const [loading, setLoading] = useState(false)

  const handleGroup = (userToAdd: User) => {
    if (selectedUsers.includes(userToAdd)) {
      toast({
        title: 'User already added',
        status: 'warning',
        duration: 5000,
        isClosable: true,
        position: 'top',
      })
      return
    }

    setSelectedUsers([...selectedUsers, userToAdd])
  }

  const handleSearch = async (query: string) => {
    try {
      setLoading(true)
      const data = await getUsers(query, user?.token as string)
      console.log(data, 'searchResult')

      setSearchResult(data)
      setLoading(false)
    } catch (error) {
      toast({
        title: 'Error Occured!',
        description: 'Failed to Load the Search Results',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'bottom-left',
      })
    }
  }

  const handleDelete = (delUser: User) => {
    setSelectedUsers(selectedUsers.filter((sel) => sel._id !== delUser._id))
  }

  const handleSubmit = async () => {
    try {
      const data = await createChatGroup(
        { name: groupChatName, users: selectedUsers },
        user?.token as string
      )

      setSelectedChat(data)
      setFetchAgain(!fetchAgain)
      onClose()
    } catch (error: any) {
      toast(toastConfig(error.message, 'error'))
    }
  }

  return (
    <>
      <span onClick={onOpen}>{children}</span>

      <Modal onClose={onClose} isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            fontSize='35px'
            fontFamily='Work sans'
            display='flex'
            justifyContent='center'
          >
            Create Group Chat
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody display='flex' flexDir='column' alignItems='center'>
            <FormControl>
              <Input
                placeholder='Chat Name'
                mb={3}
                onChange={(e) => setGroupChatName(e.target.value)}
              />
            </FormControl>
            <FormControl>
              <Input
                placeholder='Add Users eg: John, Piyush, Jane'
                mb={1}
                onChange={(e) => handleSearch(e.target.value)}
              />
            </FormControl>
            <Box w='100%' display='flex' flexWrap='wrap'>
              {selectedUsers.map((u: User) => (
                <UserBadgeItem
                  key={u._id}
                  user={u}
                  handleFunction={() => handleDelete(u)}
                />
              ))}
            </Box>
            {loading ? (
              <ChatLoading stack={3} />
            ) : (
              searchResult
                ?.slice(0, 4)
                .map((user: User) => (
                  <UserListItem
                    key={user._id}
                    user={user}
                    handleFunction={() => handleGroup(user)}
                  />
                ))
            )}
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleSubmit} colorScheme='blue'>
              Create Chat
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default GroupChatModal
