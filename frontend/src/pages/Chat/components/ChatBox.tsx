import React from 'react'
// Components
import { Box } from '@chakra-ui/layout'
import { chatState } from '@/context/ChatProvider'
import SingleChat from './SingleChat'

export interface ChatBoxProps {
  fetchAgain: boolean
  setFetchAgain: any
}

const ChatBox: React.FC<ChatBoxProps> = ({ fetchAgain, setFetchAgain }) => {
  const { selectedChat } = chatState()

  return (
    <Box
      display={{ base: selectedChat ? 'flex' : 'none', md: 'flex' }}
      alignItems='center'
      flexDir='column'
      p={3}
      bg='white'
      w={{ base: '100%', md: '68%' }}
      borderRadius='lg'
      borderWidth='1px'
    >
      {/* */}
      <SingleChat fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />
    </Box>
  )
}

export default ChatBox
