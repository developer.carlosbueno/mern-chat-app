import { Skeleton, Stack } from '@chakra-ui/react'
import React from 'react'

export interface ChatLoadingProp {
  stack?: number
}

const ChatLoading: React.FC<ChatLoadingProp> = ({ stack }) => {
  return (
    <Stack>
      {stack
        ? Array.from(Array(stack)).map(() => <Skeleton height='45px' />)
        : Array.from(Array(7)).map(() => <Skeleton height='45px' />)}
    </Stack>
  )
}

export default ChatLoading
