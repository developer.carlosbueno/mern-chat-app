import {
  isLastMessage,
  isSameSender,
  isSameSenderMargin,
  isSameUser,
} from "@/config/chatLogin";
import { chatState } from "@/context/ChatProvider";
import { Avatar, Tooltip } from "@chakra-ui/react";
import React from "react";
//@ts-ignore
import ScrollableFeed from "react-scrollable-feed";

export interface ScrollableChatProps {
  messages: any;
}

const ScrollableChat: React.FC<ScrollableChatProps> = ({ messages }) => {
  const { user } = chatState();

  return (
    <ScrollableFeed>
      {messages &&
        messages.map((m: any, i: number) => (
          <div style={{ display: "flex" }} key={m._id}>
            {(isSameSender(messages, m, i, user?._id as string) ||
              isLastMessage(messages, i, user?._id as string)) && (
              <Tooltip label={m.sender.name} placement="bottom-start" hasArrow>
                <Avatar
                  mt="7px"
                  mr={1}
                  size="sm"
                  cursor="pointer"
                  name={m.sender.name}
                  src={m.sender.pic}
                />
              </Tooltip>
            )}
            <span
              style={{
                backgroundColor: `${
                  m.sender._id === user?._id ? "#BEE3F8" : "#B9F5D0"
                }`,
                marginLeft: isSameSenderMargin(
                  messages,
                  m,
                  i,
                  user?._id as string
                ),
                marginTop: isSameUser(messages, m, i) ? 3 : 10,
                borderRadius: "20px",
                padding: "5px 15px",
                maxWidth: "75%",
              }}
            >
              {m.content}
            </span>
          </div>
        ))}
    </ScrollableFeed>
  );
};

export interface ScrollableChatProps {}

export default ScrollableChat;
