import React, { useEffect, useState } from "react";
import { User } from "@/models/User";

import { IconButton, Spinner, useToast } from "@chakra-ui/react";
import { FormControl } from "@chakra-ui/form-control";
import { Input } from "@chakra-ui/input";
import { Box, Text } from "@chakra-ui/layout";
import { ArrowBackIcon } from "@chakra-ui/icons";
import { chatState } from "@/context/ChatProvider";
import { getSender, getSenderFull } from "@/config/chatLogin";
import ProfileModal from "@/components/ProfileModal";
import animationData from "@/animation/typing.json";
import Lottie from "react-lottie";
import UpdateGroupChatModal from "./UpdateGroupChatModal";
import ScrollableChat from "./ScrollableChat";
import { createMessage, getMessages } from "../services";
import { toastConfig } from "@/utils/toastConfig";
import { io } from "socket.io-client";

let ENDPOINT = "http://localhost:3000";
let socket: any, selectedChatCompare: any;

export interface SingleChatProps {
  fetchAgain: boolean;
  setFetchAgain: any;
}

const SingleChat: React.FC<SingleChatProps> = ({
  fetchAgain,
  setFetchAgain,
}) => {
  const {
    selectedChat,
    setSelectedChat,
    user,
    notifications,
    setNotifications,
  } = chatState();

  const [messages, setMessages] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [newMessage, setNewMessage] = useState("");
  const [socketConnected, setSocketConnected] = useState(false);
  const [typing, setTyping] = useState(false);
  const [isTyping, setIsTyping] = useState(false);
  const toast = useToast();

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  useEffect(() => {
    socket = io(ENDPOINT);
    socket.emit("setup", user);
    socket.on("connected", () => setSocketConnected(true));
    socket.on("typing", (room: any) => setIsTyping(true));
    socket.on("stop typing", () => setIsTyping(false));
  }, []);

  useEffect(() => {
    handleGetMessages();

    selectedChatCompare = selectedChat;
  }, [selectedChat]);

  useEffect(() => {
    socket.on("message recieved", (message: any) => {
      setFetchAgain(!fetchAgain);

      if (
        !selectedChatCompare ||
        selectedChatCompare?._id !== message.chat._id
      ) {
        if (!notifications.includes(message)) {
          setNotifications([message, ...notifications]);
          return;
        }
      } else {
        setMessages([...messages, message]);
      }
    });
  });

  const handleGetMessages = async () => {
    if (!selectedChat) return;
    try {
      setLoading(true);
      const data = await getMessages(selectedChat?._id, user?.token as string);
      setMessages(data);
      setLoading(false);
      socket.emit("leave chat", selectedChat._id);
      socket.emit("join chat", selectedChat._id);
    } catch (error: any) {
      toast(toastConfig(error.message, "error"));
      setLoading(false);
    }
  };

  const handleCreateMessage = async (event: any) => {
    if (!selectedChat) return;
    if (event.key === "Enter" && newMessage) {
      socket.emit("stop typing", selectedChat?._id);
      try {
        const data = await createMessage(
          { chat: selectedChat._id, content: newMessage },
          user?.token as string
        );

        socket.emit("new message", data);
        setMessages([...messages, data]);
        setNewMessage("");
        setFetchAgain(!fetchAgain);
      } catch (error: any) {
        toast(toastConfig(error.message, "error"));
      }
    }
  };

  const typingHandler = (e: any) => {
    setNewMessage(e.target.value);

    if (!socketConnected) return;

    if (!typing) {
      setTyping(true);
      socket.emit("typing", selectedChat?._id);
    }

    let lastTypingTime = new Date().getTime();
    var timerLength = 3000;
    setTimeout(() => {
      var timeNow = new Date().getTime();
      var timeDiff = timeNow - lastTypingTime;
      if (timeDiff >= timerLength && typing) {
        socket.emit("stop typing", selectedChat?._id);
        setTyping(false);
      }
    }, timerLength);
  };

  return (
    <>
      {selectedChat ? (
        <>
          <Text
            fontSize={{ base: "28px", md: "30px" }}
            pb={3}
            px={2}
            w="100%"
            fontFamily="Work sans"
            display="flex"
            justifyContent={{ base: "space-between" }}
            alignItems="center"
          >
            <IconButton
              aria-label="buttom"
              display={{ base: "flex", md: "none" }}
              icon={<ArrowBackIcon />}
              onClick={() => setSelectedChat("")}
            />
            {messages &&
              (!selectedChat.isGroupChat ? (
                <>
                  {getSender(user as User, selectedChat.users)}
                  <ProfileModal
                    user={getSenderFull(user as User, selectedChat.users)}
                  />
                </>
              ) : (
                <>
                  {selectedChat.chatName.toUpperCase()}
                  <UpdateGroupChatModal
                    // fetchMessages={fetchMessages}
                    fetchAgain={fetchAgain}
                    setFetchAgain={setFetchAgain}
                  />
                </>
              ))}
          </Text>
          <Box
            display="flex"
            flexDir="column"
            justifyContent="flex-end"
            p={3}
            bg="#E8E8E8"
            w="100%"
            h="100%"
            borderRadius="lg"
            overflowY="hidden"
          >
            {loading ? (
              <Spinner
                size="xl"
                w={20}
                h={20}
                alignSelf="center"
                margin="auto"
              />
            ) : (
              <div className="messages">
                <ScrollableChat messages={messages} />
              </div>
            )}

            <FormControl
              onKeyDown={handleCreateMessage}
              id="first-name"
              isRequired
              mt={3}
            >
              {isTyping ? (
                <div>
                  <Lottie
                    options={defaultOptions}
                    // height={50}
                    width={70}
                    style={{
                      marginBottom: 15,
                      marginLeft: 0,
                      borderRadius: 15,
                    }}
                  />
                </div>
              ) : (
                <></>
              )}
              <Input
                variant="filled"
                bg="#E0E0E0"
                placeholder="Enter a message.."
                value={newMessage}
                onChange={typingHandler}
              />
            </FormControl>
          </Box>
        </>
      ) : (
        // to get socket.io on same page
        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          h="100%"
        >
          <Text fontSize="3xl" pb={3} fontFamily="Work sans">
            Click on a user to start chatting
          </Text>
        </Box>
      )}
    </>
  );
};

export default SingleChat;
