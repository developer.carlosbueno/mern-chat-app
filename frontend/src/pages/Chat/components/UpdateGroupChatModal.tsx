import React, { useState } from 'react'

// Components
import { ViewIcon } from '@chakra-ui/icons'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  useDisclosure,
  FormControl,
  Input,
  useToast,
  Box,
  IconButton,
  Spinner,
} from '@chakra-ui/react'
import UserBadgeItem from './UserBadgeItem'
import UserListItem from './UserListItem'

import { chatState } from '@/context/ChatProvider'
import { User } from '@/models/User'
import {
  AddUserToChatGroup,
  getUsers,
  removeUserFromChatGroup,
  renameGroup,
} from '../services'
import { toastConfig } from '@/utils/toastConfig'

export interface UpdateGroupChatModalProps {
  setFetchAgain: any
  fetchAgain: boolean
}

const UpdateGroupChatModal: React.FC<UpdateGroupChatModalProps> = ({
  setFetchAgain,
  fetchAgain,
}) => {
  const { selectedChat, setSelectedChat, user } = chatState()

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [groupChatName, setGroupChatName] = useState('')
  const [search, setSearch] = useState('')
  const [searchResult, setSearchResult] = useState<User[]>([])
  const [loading, setLoading] = useState(false)
  const [renameloading, setRenameLoading] = useState(false)
  const toast = useToast()

  const handleRemove = async (u: User) => {
    try {
      setLoading(true)
      const updatedChat = await removeUserFromChatGroup(
        u._id,
        selectedChat?._id as string,
        user?.token as string
      )

      setSelectedChat(updatedChat)
      setFetchAgain(!fetchAgain)
      setLoading(false)
    } catch (error: any) {
      toast(toastConfig(error.message, 'error'))
    }
  }

  const handleRename = async (u: any) => {
    try {
      setLoading(true)
      const updatedChat = await renameGroup(
        selectedChat?._id as string,
        groupChatName,
        user?.token as string
      )

      setSelectedChat(updatedChat)
      setLoading(false)
      setFetchAgain(!fetchAgain)
    } catch (error: any) {
      toast(toastConfig(error.message, 'error'))
    }
  }

  const handleAddUser = async (u: User) => {
    if (selectedChat?.users.find((i) => i._id === u._id)) {
      toast(toastConfig('User already in group', 'warning'))
      return
    }

    if (selectedChat?.groupAdmin._id !== user?._id) {
      toast(toastConfig('Only admin can add someone in the group', 'warning'))
      return
    }
    try {
      setLoading(true)
      const updatedChat = await AddUserToChatGroup(
        u._id,
        selectedChat?._id as string,
        user?.token as string
      )

      setSelectedChat(updatedChat)
      setFetchAgain(!fetchAgain)
      setLoading(false)
    } catch (error: any) {
      toast(toastConfig(error.message, 'error'))
    }
  }

  const handleSearch = async (query: string) => {
    if (!query) {
      toast({
        title: 'Please Enter something in search',
        status: 'warning',
        duration: 5000,
        isClosable: true,
        position: 'top-left',
      })
      return
    }

    try {
      setLoading(true)
      console.log(user?.token, 'user token search')

      const users = await getUsers(search, user?.token as string)
      setSearchResult(users)
      setLoading(false)
    } catch (error: any) {
      toast(toastConfig(error.message, 'error'))
      setLoading(false)
    }
  }

  return (
    <>
      <IconButton
        aria-label='ok'
        display={{ base: 'flex' }}
        icon={<ViewIcon />}
        onClick={onOpen}
      />

      <Modal onClose={onClose} isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            fontSize='35px'
            fontFamily='Work sans'
            display='flex'
            justifyContent='center'
          >
            {selectedChat?.chatName}
          </ModalHeader>

          <ModalCloseButton />
          <ModalBody display='flex' flexDir='column' alignItems='center'>
            <Box w='100%' display='flex' flexWrap='wrap' pb={3}>
              {selectedChat?.users.map((u: User) => (
                <UserBadgeItem
                  key={u._id}
                  user={u}
                  admin={selectedChat?.groupAdmin}
                  handleFunction={() => handleRemove(u)}
                />
              ))}
            </Box>
            <FormControl display='flex'>
              <Input
                placeholder='Chat Name'
                mb={3}
                value={groupChatName}
                onChange={(e) => setGroupChatName(e.target.value)}
              />
              <Button
                variant='solid'
                colorScheme='teal'
                ml={1}
                isLoading={renameloading}
                onClick={handleRename}
              >
                Update
              </Button>
            </FormControl>
            <FormControl>
              <Input
                placeholder='Add User to group'
                mb={1}
                onChange={(e) => handleSearch(e.target.value)}
              />
            </FormControl>

            {loading ? (
              <Spinner size='lg' />
            ) : (
              searchResult?.map((user: User) => (
                <UserListItem
                  key={user._id}
                  user={user}
                  handleFunction={() => handleAddUser(user)}
                />
              ))
            )}
          </ModalBody>
          <ModalFooter>
            <Button
              onClick={() => handleRemove(user as User)}
              colorScheme='red'
            >
              Leave Group
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default UpdateGroupChatModal
