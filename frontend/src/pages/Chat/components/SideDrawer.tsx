import React, { useState } from "react";
import { chatState } from "@/context/ChatProvider";

// Icons
import { HiOutlineSearch } from "react-icons/hi";
import { CiBellOn } from "react-icons/ci";
import { AiOutlineDown } from "react-icons/ai";

// Components
import {
  Avatar,
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Input,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Spinner,
  Text,
  Tooltip,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import ProfileModal from "@/components/ProfileModal";
import { useNavigate } from "react-router-dom";
import { User } from "@/models/User";
import { accessChat, getUsers } from "../services";
import { toastConfig } from "@/utils/toastConfig";
import ChatLoading from "./ChatLoading";
import UserListItem from "./UserListItem";
import { getSender } from "@/config/chatLogin";

// @ts-ignore
import NotificationBadge, { Effect } from "react-notification-badge";

export interface SideDrawerProps {}

const SideDrawer: React.FC<SideDrawerProps> = () => {
  const navigate = useNavigate();
  const {
    user,
    chats,
    setChats,
    setSelectedChat,
    notifications,
    setNotifications,
  } = chatState();
  const toast = useToast();

  console.log(user, "my user");

  const { isOpen, onOpen, onClose } = useDisclosure();

  const [search, setSearch] = useState("");
  const [searchResult, setSearchResult] = useState<User[]>([]);
  const [loading, setLoading] = useState(false);
  const [loadingChat, setLoadingChat] = useState(false);

  const handlerLogout = () => {
    localStorage.removeItem("user");
    navigate("/");
  };

  const handleSearch = async () => {
    if (!search) {
      toast({
        title: "Please Enter something in search",
        status: "warning",
        duration: 5000,
        isClosable: true,
        position: "top-left",
      });
      return;
    }

    try {
      setLoading(true);
      console.log(user?.token, "user token search");

      const users = await getUsers(search, user?.token as string);
      setSearchResult(users);
      setLoading(false);
    } catch (error: any) {
      toast(toastConfig(error.message, "error"));
      setLoadingChat(false);
    }
  };

  const handleAccessChat = async (id: string) => {
    try {
      setLoadingChat(true);
      const data = await accessChat(id, user?.token as string);

      if (!chats.find((c: any) => c._id === data._id))
        setChats([data, ...chats]);
      onClose();
      setLoadingChat(false);
    } catch (error: any) {
      toast(toastConfig(error.message, "error"));
      setLoadingChat(false);
    }
  };

  console.log(notifications, "Notificaitons");

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      bg="white"
      w="100%"
      p="5px 10px 5px 10px"
      borderWidth="5px"
    >
      <Tooltip label="Search users to chat" hasArrow placement={"bottom-end"}>
        <Button variant={"ghost"} onClick={onOpen}>
          <HiOutlineSearch />
          <Text p={{ base: "none", md: "flex" }} padding="10px">
            Search user
          </Text>
        </Button>
      </Tooltip>

      <Text fontSize="2xl" fontFamily="Work sans">
        Talk-A-Tive
      </Text>

      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Menu>
          <MenuButton p={1}>
            <NotificationBadge
              count={notifications.length}
              effect={Effect.SCALE}
            />
            <CiBellOn fontSize={35} style={{ margin: 1 }} />
          </MenuButton>
          <MenuList pl={2}>
            {!notifications.length && "No New Messages"}
            {notifications.map((notif: any) => (
              <MenuItem
                key={notif._id}
                onClick={() => {
                  setSelectedChat(notif.chat);
                  setNotifications(
                    notifications.filter((n: any) => n !== notif)
                  );
                }}
              >
                {notif.chat.isGroupChat
                  ? `New Message in ${notif.chat.chatName}`
                  : `New Message from ${getSender(
                      user as User,
                      notif.chat.users
                    )}`}
              </MenuItem>
            ))}
          </MenuList>
        </Menu>
        <Menu>
          <MenuButton as={Button} bg="white" rightIcon={<AiOutlineDown />}>
            <Avatar
              size="sm"
              cursor="pointer"
              name={user?.name}
              src={user?.picture}
            />
          </MenuButton>
          <MenuList>
            <ProfileModal user={user as User}>
              <MenuItem>My Profile</MenuItem>
            </ProfileModal>
            <MenuDivider />
            <MenuItem onClick={handlerLogout}>Logout</MenuItem>
          </MenuList>
        </Menu>
      </Box>

      <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader borderBottomWidth="1px">Search Users</DrawerHeader>
          <DrawerBody>
            <Box display="flex" pb={2}>
              <Input
                placeholder="Search by name or email"
                mr={2}
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
              <Button onClick={handleSearch}>Go</Button>
            </Box>
            {loading ? (
              <ChatLoading />
            ) : (
              searchResult?.map((user) => (
                <UserListItem
                  key={user._id}
                  user={user}
                  handleFunction={() => handleAccessChat(user._id)}
                />
              ))
            )}
            {loadingChat && <Spinner ml="auto" display="flex" />}
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </Box>
  );
};

export default SideDrawer;
