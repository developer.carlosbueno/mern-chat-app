import React, { useEffect, useState } from 'react'
import styles from './styles/Chat.module.css'

// Services
import { chatState } from '@/context/ChatProvider'
import { ChatBox, MyChats, SideDrawer } from './components'
import { Box } from '@chakra-ui/react'
import { getAllChats } from './services'

export interface ChatProps {}

const Chat: React.FC<ChatProps> = () => {
  const { user } = chatState()
  const [fetchAgain, setFetchAgain] = useState(false)

  // useEffect(() => {
  //   getAllChats(user?.token as string)
  // }, [fetchAgain])

  return (
    <>
      {user ? (
        <div className={styles.chat}>
          <SideDrawer />
          <Box
            display='flex'
            justifyContent='center'
            w='100%'
            h='91.5vh'
            p='1rem'
          >
            <MyChats fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />
            <ChatBox fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />
          </Box>
        </div>
      ) : null}
    </>
  )
}

export default Chat
