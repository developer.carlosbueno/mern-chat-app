import axios from "@/utils/axios";

// Models
import { Response } from "@/models/Response";
import { User } from "@/models/User";

const createConfig = (user: string, post?: boolean) => {

  let config = {
    'Content-type': post ? 'application/json' : '',
    headers: {
      Authorization: 'Bearer ' + user
    }
  }

  return config
}

export const getUsers = async (search: string, user: string): Promise<User[]> => {
  console.log(user, 'FROM GET USER TOKEN');

  let config = createConfig(user)

  try {
    const { data } = await axios.get<Response<User[]>>(`/api/user?search=${search}`, config)
    return data.body
  } catch (error: any) {
    throw new Error(error.response.data.body || error.message)
  }
}

export const getAllChats = async (user: string) => {
  let config = createConfig(user)

  try {
    const { data } = await axios.get("/api/chat", config);
    return data.body
  } catch (error: any) {
    console.log(error.message);
  }
};

export const accessChat = async (userId: string, user: string) => {

  let config = createConfig(user, true)

  try {
    const { data } = await axios.post('/api/chat', { userId }, config)
    return data.body
  } catch (error: any) {
    console.log('ACCESS CHAT ERROR: ', error.message)
  }
}

export const createChatGroup = async (body: any, user: string) => {

  let config = createConfig(user, true)

  try {
    const { data } = await axios.post('/api/chat/group', body, config)
    return data.body
  } catch (error: any) {
    console.log('CREATE CHAT ERROR: ', error.message)
  }
}

export const AddUserToChatGroup = async (userId: string, chatId: string, token: string) => {

  let config = createConfig(token, true)

  try {
    const { data } = await axios.patch('/api/chat/group-add', { chatId, userId }, config)
    return data.body
  } catch (error: any) {
    console.log('ADD USER ERROR: ', error.message)
  }
}

export const removeUserFromChatGroup = async (userId: string, chatId: string, token: string) => {

  let config = createConfig(token, true)

  try {
    const { data } = await axios.patch('/api/chat/group-remove', { chatId, userId }, config)
    return data.body
  } catch (error: any) {
    console.log('REMOVE USER ERROR: ', error.message)
  }
}

export const renameGroup = async (chatId: string, name: string, token: string) => {

  let config = createConfig(token, true)

  try {
    const { data } = await axios.patch('/api/chat/rename', { chatId, name }, config)
    return data.body
  } catch (error: any) {
    console.log('RENAME CHAT ERROR: ', error.message)
  }
}


export const createMessage = async (body: any, token: string) => {

  let config = createConfig(token, true)

  try {
    const { data } = await axios.post('/api/messages', { ...body }, config)
    return data.body
  } catch (error: any) {
    console.log('CREATE MESSAGE ERROR: ', error.message)
  }
}

export const getMessages = async (chatId: string, token: string) => {

  let config = createConfig(token)

  try {
    const { data } = await axios.get(`/api/messages/${chatId}`, config)
    return data.body
  } catch (error: any) {
    console.log('GET MESSAGES ERROR: ', error.message)
  }
}
